/*
 * debounce.h
 *
 * Created: 2/29/2020 1:24:14 AM
 * Author: UniX
 */

#ifndef DEBOUNCE_H_
#define DEBOUNCE_H_

// dependencies:
#include "linkedlist.h"
#include <stdint.h>

void
debounce_button_state_changed (uint8_t button_index);

_Bool
debounce_is_button_debounced (uint8_t button_index);

#endif /* DEBOUNCE_H_ */