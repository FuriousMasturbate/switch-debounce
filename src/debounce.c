/*
 * debounce.c v1.0
 * contains debounce function that is capable of dealing with only 1 button
 * can be expanded into a multi-button debounce situation using ARRAYS for
 * buttonpin_key and buttonpin_value
 *
 * Created: 2/16/2020 3:32:06 PM
 * Author : UniX
 */

#include "debounce.h"
#include "debounce_listhandler.h"

#define BUTTON_STATE_CHANGED 0
#define BUTTON_STATE_STABLE 1

static void set_button_state_to_stable (button_index);

void
debounce_button_state_changed (uint8_t button_index)
{
	node_content_t node_content = { .index = button_index,
		                            .data = BUTTON_STATE_CHANGED };
	listhandler_auto_update_node (node_content);
}



_Bool
debounce_is_button_debounced (uint8_t button_index)
{
	_Bool button_state_has_been_stable_since_last_check =
	  listhandler_get_node_data (button_index);

	if (button_state_has_been_stable_since_last_check)
	{
		delete_node_from_memory(button_index);
		return 1;
	}
	else
		set_button_state_to_stable (button_index);
		return 0;
}

static void set_button_state_to_stable (button_index)
{
	node_content_t node_content = { .index = button_index,
		                            .data = BUTTON_STATE_STABLE };
	listhandler_auto_update_node (node_content);
}