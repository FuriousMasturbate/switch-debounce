#ifndef LIST_HANDLER_H_
#define LIST_HANDLER_H_

#include "singly-linked-list/linkedlist-global-definitions.h"

typedef struct
{
	node_index_type index;
	node_data_type data;
} node_content_t;

void
listhandler_auto_update_node (node_content_t node_content);

node_data_type listhandler_get_node_data (button_index);

void delete_node_from_memory (button_index);

#endif /* LIST_HANDLER_H_ */
