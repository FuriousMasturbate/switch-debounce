#include "debounce_listhandler.h"
#include "singly-linked-list/linkedlist.h"

list_node* debounce_list_head = NULL;

void
listhandler_auto_update_node (node_content_t node_content)
{
	node_unique_id node_uid = { .address_of_list_head = &debounce_list_head,
		                        .index = node_content.index };
	list_auto_update_node (node_uid, node_content.data);
}



node_data_type listhandler_get_node_data (button_index)
{
	node_unique_id node_uid = { .address_of_list_head = &debounce_list_head,
		                        .index = button_index };
	return list_find_node (node_uid);
}


void delete_node_from_memory (button_index)
{
	node_unique_id node_uid = { .address_of_list_head = &debounce_list_head,
		                        .index = button_index };
	list_delete_node (node_uid);
}